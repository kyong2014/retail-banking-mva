import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView, Platform, Image, StatusBar } from 'react-native';
import { Root, Icon, Container, Header, Content, Body } from 'native-base';
import { StackNavigator, DrawerNavigator, DrawerItems } from 'react-navigation';

import LoginScreen from './app/login/LoginScreen';
import LoginWithEmailScreen from './app/login/LoginWithEmailScreen';
import RegisterScreen from './app/login/RegisterScreen';

import MVAScreen from './app/mva/MVAScreen';
import MVA from './app/mva/MVA';

import TransactionScreen from './app/banking/TransactionScreen';

import ProfileScreen from './app/profile/ProfileScreen';

import SplashScreen from './app/screens/SplashScreen';
import SettingsScreen from './app/screens/SettingsScreen';
import ProductServiceScreen from './app/product-service/ProductServiceScreen';

import DrawerContent from './app/components/DrawerContent';

var v = require('./app/stylesheets/variables');

StatusBar.setHidden(true);

const DrawerNavigation = DrawerNavigator(
  {
    Transactions: {
      screen: TransactionScreen
    },
    MVA: {
      screen: MVA
    },
    Products_Services:{
      screen: ProductServiceScreen
    },
  },
  {
    initialRouteName: 'MVA',
    drawerPosition: 'left',
    contentComponent: DrawerContent,
    contentOptions: {
      activeTintColor: v.HASTE_COLOR,
    }
  }
);

const StackNavigation = StackNavigator({
  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: {
        gesturesEnabled: false,
    }
  },
  LoginWithEmailScreen: {
    screen: LoginWithEmailScreen
  },
  RegisterScreen: {
    screen: RegisterScreen
  },
  Home: {
    screen: DrawerNavigation,
    navigationOptions: {
        gesturesEnabled: false,
    }
  },

  MVAScreen: {
    screen: MVAScreen,
    navigationOptions: {
      gesturesEnabled: false,
    }
  },
},
{
    headerMode: 'none'
  }
);


const CustomDrawerContentComponent = (props) => (
  <Container>
    <Header>
      <Body>
        
      </Body>
    </Header>
  </Container>
)


export default () => <Root><StackNavigation/></Root>;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


