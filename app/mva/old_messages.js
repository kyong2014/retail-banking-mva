module.exports = [
  {
    _id: Math.round(Math.random() * 1000000),
    text:
      "You can use voice function to engage with the different HSBC banking services. Alternatively, you can contact our Call Center for any further enquiries.",
    createdAt: new Date(Date.UTC(2018, 2, 1, 7, 20, 0)),
    user: {
      _id: 1,
      name: "Anita Suresh"
    }
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: "Welcome to HSBC Mobile Banking! I am Bob, your Mobile Virtual Assistant (MVA).",
    createdAt: new Date(Date.UTC(2018, 2, 1, 7, 20, 0)),
    user: {
      _id: 1,
      name: "Anita Suresh"
    }
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: "This is a system message.",
    createdAt: new Date(Date.UTC(2018, 2, 1, 7, 18, 0)),
    system: true
  }
];