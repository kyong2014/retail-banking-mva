import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { Icon, Container, Content, Left, Right, List, ListItem, Button, connectStyle, Footer, Toast } from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CustomHeader from '../components/CustomHeader';

import { GiftedChat, Actions, Bubble, SystemMessage } from 'react-native-gifted-chat';
import CustomActions from './CustomActions';
import CustomView from './CustomView';

import { Bot } from './bot.js';

export default class MVA extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'MVA',
    drawerIcon: ({ tintColor }) => {
      return (
        <MaterialIcons
          name="record-voice-over"
          size={24}
          style={{ color: tintColor }}
        >
        </MaterialIcons>
      );
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: true,
      typingText: null,
      isLoadingEarlier: false,
    };

    this._isMounted = false;
    this.onSend = this.onSend.bind(this);
    this.onReceive = this.onReceive.bind(this);
    this.renderCustomActions = this.renderCustomActions.bind(this);
    this.renderBubble = this.renderBubble.bind(this);
    this.renderSystemMessage = this.renderSystemMessage.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.onLoadEarlier = this.onLoadEarlier.bind(this);

  }

  componentWillMount() {
    this._isMounted = true;
    this.setState(() => {
      return {
        messages: [{
          _id: Math.round(Math.random() * 1000000),
          text: 'Welcome to HSBC Mobile Banking! I am Bob, your Mobile Virtual Assistant (MVA).',
          createdAt: new Date(Date.UTC(2018, 2, 19, 9, 32, 0)),
          user: {
            _id: 2,
            name: 'Bob',
          },
        },
        {
          _id: Math.round(Math.random() * 1000000),
          text: "You are currently talking to Bob, your personal MVA.",
          createdAt: new Date(Date.UTC(2018, 2, 19, 9, 30, 0)),
          system: true,
        },],
      };
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onLoadEarlier() {
    this.setState((previousState) => {
      return {
        isLoadingEarlier: true,
      };
    });

    setTimeout(() => {
      if (this._isMounted === true) {
        this.setState((previousState) => {
          return {
            messages: GiftedChat.prepend(previousState.messages, require('./old_messages.js')),
            loadEarlier: false,
            isLoadingEarlier: false,
          };
        });
      }
    }, 1000); // simulating network
  }

  onSend(messages = []) {
    console.log('onSend', messages);
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });

    // for demo purpose
    this.answerDemo(messages);
  }

  answerDemo(messages) {
    if (messages.length == 0) {
      return;
    }
    if (this._isMounted === true) {
      if (messages.length > 0) {
        if (messages[0].image) {
          this.answer('Nice picture!');
        } else if (messages[0].location) {
          this.answer('My favorite place');
        } else {
          const responses = Bot.listen(messages[0]);
          responses.forEach((response) => {
            this.answer(response.text, response.delay, response.system, response.typer);
          })
        }
      }
    }
  }

  answer(text, delay = 0, system = false, typer = 'Bob (MVA)') {
    setTimeout(() => {
      if (!system) {
        this.setState((previousState) => {
          return {
            typingText: typer + ' is typing'
          };
        });
      }

      setTimeout(() => {

        this.onReceive(text, system);

        this.setState((previousState) => {
          return {
            typingText: null,
          };
        });
      }, 1000);
    }, delay)


  }

  onReceive(text, system = false) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: text,
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'Bob:'
          },
          system
        }),
      };
    });
  }

  renderCustomActions(props) {
    if (Platform.OS === 'ios') {
      return (
        <CustomActions
          {...props}
        />
      );
    }
    const options = {
      'Action 1': (props) => {
        alert('option 1');
      },
      'Action 2': (props) => {
        alert('option 2');
      },
      'Cancel': () => { },
    };
    return (
      <Actions
        {...props}
        options={options}
      />
    );
  }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: '#f0f0f0',
          }
        }}
      />
    );
  }

  renderSystemMessage(props) {
    return (
      <SystemMessage
        {...props}
        containerStyle={{
          marginBottom: 15,
        }}
        textStyle={{
          fontSize: 14,
        }}
      />
    );
  }

  renderCustomView(props) {
    return (
      <CustomView
        {...props}
      />
    );
  }

  renderFooter(props) {
    if (this.state.typingText) {
      return (
        <View style={styles.footerContainer}>
          <Text style={styles.footerText}>
            {this.state.typingText}
          </Text>
        </View>
      );
    }
    return null;
  }
  sendOnEnter(text) {
    if (text.includes('\n')) {
      // this.onSend(text);
    }
  }

  render() {
    return (
      <Container>
        <CustomHeader menu='yes' callCenter='yes' msgStaff='yes'
          name="Mobile Virtual Assistant"
          showName={true} nav={this.props.navigation}
        />

        <GiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          loadEarlier={this.state.loadEarlier}
          onLoadEarlier={this.onLoadEarlier}
          isLoadingEarlier={this.state.isLoadingEarlier}

          user={{
            _id: 1, // sent messages should have same user._id
          }}
          // onInputTextChanged={this.sendOnEnter}
          renderActions={this.renderCustomActions}
          renderBubble={this.renderBubble}
          renderSystemMessage={this.renderSystemMessage}
          renderCustomView={this.renderCustomView}
          renderFooter={this.renderFooter}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  footerContainer: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
  },
  footerText: {
    fontSize: 14,
    color: '#aaa',
  },
});