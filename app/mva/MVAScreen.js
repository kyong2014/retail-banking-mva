import React from 'react';
import { Text, View, ScrollView, AsyncStorage, TouchableOpacity, StyleSheet, FlatList, Image, Alert, BackHandler } from 'react-native';
import {Icon, Container, Content, Left, Right, List, ListItem, Button, connectStyle, Footer, Toast } from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CustomHeader from '../components/CustomHeader';
import LoadingIndicator from '../components/LoadingIndicator';
import MVA from './MVA.js';
import { AppLoading, Font } from 'expo';
import {WINDOW_WIDTH} from '../stylesheets/variables';

var api = require('../../api');

const util = require('util');

var v = require('../stylesheets/variables');
var style_theme = require('../stylesheets/theme');

class MVAScreen extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'FAQ',
        drawerIcon: ({tintColor}) => {
            return (
                <MaterialIcons
                name="record-voice-over"
                size={24}
                style={{color: tintColor}}
                >
                </MaterialIcons>
            );
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            fontLoaded: false,
        };
    }

    async componentDidMount() {
        await Font.loadAsync({
          'Roboto_medium': require('../resources/fonts/Roboto/Roboto-Medium.ttf'),  
          'Open_Sans': require('../resources/fonts/Open_Sans/OpenSans-Regular.ttf'),
          'Open_Sans_bold': require('../resources/fonts/Open_Sans/OpenSans-Bold.ttf'),
          'Open_Sans_light': require('../resources/fonts/Open_Sans/OpenSans-Light.ttf'),
        });
    
        this.setState({ fontLoaded: true });
    }

    render(){
        const { navigate } = this.props.navigation;
        return(
            this.state.fontLoaded ? (
            <Container>
                <CustomHeader menu='yes' callCenter='yes' msgStaff='yes' name="Mobile Virtual Assistant" showName={true} nav={this.props.navigation} />
                <Content>
                    <MVA navigator={this.props.navigation}/>
                </Content>
            </Container>
            ) : null
        );
        <LoadingIndicator isLoading={this.state.isLoading}/>
    }
}

styles = StyleSheet.create({
    
})

export default connectStyle('yourTheme.MVAScreen', styles)(MVAScreen);