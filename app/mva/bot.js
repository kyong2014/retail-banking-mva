import _ from 'lodash';

const Bot = {
    state: 'general',
    failCount: 0,

    listen(payload) {
        const text = payload.text
        console.log('state', this.state);
        let response = _.flatten([this.flow[this.state].parse(text)]);

        if (response && response.length > 0 && response[0] === 'fail') {
            response.splice(0, 1);
            this.failCount++;
        } else {
            this.failCount = 0;
        }

        return response;
    },
    flow: {
        'general': {
            parse(text) {
                // Empty Text
                if (text.trim().length === 0) { return { text: "Sorry, I didn't get what you were trying to say" } }

                // Nearest branch
                if (/\b(nearest branch|where|how to go|how do i go)\b/i.test(text)) { return { text: "The nearest HSBC branch to you is located at 68 Orchard Road #01-60 Plaza Singapura, 238839." } }
                if (/\b(nearest atm)\b/i.test(text)) { return { text: "The nearest HSBC ATM to you is located at 68 Orchard Road #01-60 Plaza Singapura, 238839." } }
                if (/\b(nearest)\b/i.test(text)) { return { text: "Are you looking for the nearest branch or nearest ATM?" } }

                // Account Balance
                if (/\b(balance|money left|money do i have left)\b/i.test(text)) { return { text: "Your account balance is $1833.94" } }

                // TODO: FAQs

                // Live agent
                if (/\b(agent|speak to someone|talk to someone|real person|customer support)\b/i.test(text)) {
                    Bot.state = 'livechat';
                    return [
                        { text: "Please wait while we connect you to our live chat.." },
                        { text: "There are currently 2 people in the queue. Your estimated wait time is 1 minute.", delay: 2000 },
                        { text: "You are currently talking to Anita Suresh, our customer support agent", delay: 5000, system: true },
                        { text: "Hello my name is Anita Suresh. How can i help you today?", delay: 8000, typer: 'Anita Suresh (agent)' }
                    ]
                }

                if (/\b(suspect|fraud|stolen|fraudulent|scam(med)?|lost (my )?((c|d)redit )?card)\b/i.test(text)) {
                    Bot.state = 'general?livechat'
                    return [
                        { text: "Oh dear. Would you like to speak to our customer agent about it?" },
                    ]
                }

                // TODO: Loans
                if (/\b(loan|loans|borrow|need money)\b/i.test(text)) {
                    Bot.state = 'loan';
                    return { text: "You would like to apply for a personal loan?" }
                }


                // Intro
                if (/\b(what can you do|what can i ask|menu)\b/i.test(text)) { return { text: "You may ask me:\n• questions regarding HSBC bank\n• your account balance\n• apply for a loan\n• nearest branch/ATM \n• speak to our customer support agent" } }


                // Thank you
                if (/\b(thank you|thx|thanks|gratitude)\b/i.test(text)) { return { text: "No problem! I'm always here 24/7 if you need me :)" } }

                // Joke
                if (/\b(joke(s)?|something funny)\b/i.test(text)) {
                    const jokes = [
                        [{ text: "Can a kangaroo jump higher than a house?" }, { text: "Of course, a house doesn't jump at all", delay: 3000 }],
                        [{ text: "Anton, do you think I’m a bad mother?" }, { text: "My name is Paul.", delay: 3000 }],
                        [{ text: "My dog used to chase people on a bike a lot. It got so bad, finally I had to take his bike away." }],
                        [{ text: "What is the difference between a snowman and a snowwoman?" }, { text: "Snowballs.", delay: 3000 }],
                        [{ text: "Oh darling, since you’ve started dieting, you’ve become such a passionate kisser…" }, { text: "What do you mean, passionate? I’m looking for food remains!", delay: 3000 }],
                        [{ text: "You know how it is in life. One door closes – that means another door opens…" }, { text: "Yeah, very nice, but you either fix that or I’m expecting a serious discount on that car!", delay: 3000 }],
                        [{ text: "A wife complains to her husband: “Just look at that couple down the road, how lovely they are. He keeps holding her hand, kissing her, holding the door for her, why can’t you do the same?" }, { text: "The husband: “Are you mad? I barely know that woman!”", delay: 3000 }],
                        [{ text: "Patient: Oh doctor, I’m just so nervous. This is my first operation." }, { text: "Doctor: Don't worry. Mine too.", delay: 3000 }],
                        [{ text: "In a boomerang shop: I'd like to buy a new boomerang please. Also, can you tell me how to throw the old one away?" },],
                        [{ text: "Patient: Doctor help me please, every time I drink a cup of coffee I get this intense stinging in my eye." }, { text: "Doctor: I suggest you remove the spoon before drinking.", delay: 3000 }],
                        [{ text: "Men 1845: I just killed a buffalo." }, { text: "Men 1952: I just fixed the roof.", delay: 3000 }, { text: "Men 2017: I just shaved my legs.", delay: 6000 }],
                        [{ text: "Your bank account is a joke." }],
                    ]
                    return pickRandom(jokes);

                }


                // Greeting
                if (/\b(hey|yo|hello|hi|what's up|good morning)\b/i.test(text)) { return { text: "Hello, how may i help you?" } }

                // Good bye
                if (/\b(good( |-)?bye|bye bye|bye|farewell|cya|see ya|)\b/i.test(text)) { return { text: "Good bye!" } }

                // Handle many fallbacks
                if (Bot.failCount === 2) {
                    Bot.state = 'general?livechat';
                    Bot.failCount = 0;
                    return [{ text: "It seems that the bot is having difficulty understanding you. Would you like to speak to a real person instead?" }];
                }

                // Fallback
                const fallbacks = [
                    { text: "Sorry I could not find the answer to your question. Could you rephrase it?" },
                    { text: "I must be hard of hearing, could you repeat what you need?" },
                    { text: "I couldn't understand what you said. Do you mind saying it again?" },
                ]
                return ['fail', pickRandom(fallbacks)];

            }
        },
        'livechat': {
            parse(text) {
                if (/\b(good( |-)?bye|bye bye|bye|farewell|cya|see ya|end chat|end conversation|and( this)? conversation|and( this)? chat)\b/i.test(text)) {
                    Bot.state = "general"
                    return [
                        { text: "If there's nothing else, hope my help was useful. Have a good day sir.", delay: 2000, typer: 'Anita Suresh (agent)' },
                        { text: "You are currently talking to Bob, your personal MVA.", delay: 3000, system: true },
                        { text: "andddd Bob is back into the game! How can i help you?", delay: 5000 },
                    ]
                }
                if (/\b(suspect|fraud|stolen|fraudulent|scam(med)?|lost (my )?((c|d)redit )?card)\b/i.test(text)) {
                    return [
                        { text: "I am sorry to hear that. When did this happen?", delay: 5000, typer: "Anita Suresh (agent)" },
                    ]
                }

                if (/\b(yesterday|today|this (morning|afternoon|evening)|just now|(last|few|one|two|three|four|five|six|seven|several|1|2|3|4|5|6|7) (day(s)?|week(s)?|hour(s)?))\b/i.test(text)) {
                    Bot.state = "livechat?other";
                    return [
                        { text: "Ok sir. We will investigate this and update you again.", delay: 5000, typer: "Anita Suresh (agent)" },
                        { text: "Meanwhile I will cancel your card and prevent further transactions from going through.", delay: 7000, typer: "Anita Suresh (agent)" },
                        { text: "You will also receive your new card in 3 working days.", delay: 9000, typer: "Anita Suresh (agent)" },
                        { text: "Is there anything else we can help you with?", delay: 11000, typer: "Anita Suresh (agent)" },
                    ]
                }

                // Fallback
                return Bot.flow['general'].parse(text);
            }
        },
        'general?livechat': {
            parse(text) {
                if (/\b(nope|no|that(')s (all|it)|nothing|good( |-)?bye|bye bye|bye|farewell|cya|see ya|end chat|end conversation|and( this)? conversation|and( this)? chat)\b/i.test(text)) {
                    Bot.state = "general"
                    return [
                        { text: "Alright. Let me know if you need our help" },
                    ]
                }

                if (/\b(yes|yea|yup|yeap|yep)\b/i.test(text)) {
                    Bot.state = "livechat";
                    return [
                        { text: "Please wait while we connect you to our live chat.." },
                        { text: "There are currently 2 people in the queue. Your estimated wait time is 1 minute.", delay: 2000 },
                        { text: "You are currently talking to Anita Suresh, our customer support agent", delay: 5000, system: true },
                        { text: "Hello my name is Anita Suresh. How can i help you today?", delay: 8000, typer: 'Anita Suresh (agent)' }
                    ]
                }

                // Fallback
                return Bot.flow['general'].parse(text);
            }
        },
        'livechat?other': {
            parse(text) {
                if (/\b(nope|that(')s (all|it)|no|nothing|good( |-)?bye|bye bye|bye|farewell|cya|see ya|end chat|end conversation|and( this)? conversation|and( this)? chat)\b/i.test(text)) {
                    Bot.state = "general"
                    return [
                        { text: "If there's nothing else, hope my help was useful. Have a good day sir.", delay: 2000, typer: 'Anita Suresh (agent)' },
                        { text: "You are currently talking to Bob, your personal MVA.", delay: 3000, system: true },
                        { text: "andddd Bob is back into the game! How can i help you?", delay: 5000 },
                    ]
                }

                if (/\b(yes|yea|yup|yeap|yep)\b/i.test(text)) {
                    Bot.state = "livechat";
                    return [
                        { text: "What can i help you with?", delay: 5000 },
                    ]
                }

                // Fallback
                return Bot.flow['general'].parse(text);
            }
        },
        'loan': {
            parse(text) {
                if (/\b(nope|no|don(')?t want|not)\b/i.test(text)) {
                    Bot.state = "general";
                    return [
                        { text: "Oh, I thought you mentioned that you wanted a loan. My bad." }
                    ]
                }

                if (/\b(yes|yea|yup|yeap|yep)\b/i.test(text)) {
                    Bot.state = "loan?income"
                    return [
                        { text: "What is your monthly income?" }
                    ]
                }

                // Fallback to general
                return Bot.flow['general'].parse(text);
            }
        },
        'loan?income': {
            parse(text) {
                if (/\b(cancel)\b/i.test(text)) {
                    Bot.state = "general";
                    return [
                        { text: "Oh, I thought you mentioned that you wanted a loan. My bad." }
                    ]
                }

                if (/\b([1-9]{1}\d{3,}|hundred|thousand)\b/i.test(text)) {
                    Bot.state = "loan?loan_amount";
                    return [
                        { text: "How much would you like to borrow?" }
                    ]
                } else if (/\b(\d+)\b/i.test(text)) {
                    return [
                        { text: "Sorry, the amount you have entered is too low. What is your monthly income?" }
                    ]
                } else {
                    return [
                        { text: "Sorry, i didn't quite get that. What is your monthly income?" }
                    ]
                }

                // Fallback to general
            }
        },
        'loan?loan_amount': {
            parse(text) {
                if (/\b(cancel)\b/i.test(text)) {
                    Bot.state = "general";
                    return [
                        { text: "Oh, I thought you mentioned that you wanted a loan. My bad." }
                    ]
                }

                if (/\b([5-9]{1}\d{2,}|hundred|thousand)\b/i.test(text)) {
                    Bot.state = "loan?loan_tenure";
                    return [
                        { text: "How long would you like the repayment period to be?\n• 12 months\n• 24 months\n• 36 months\n• 48 months\n• 60 months" }
                    ]
                } else if (/\b(\d+)\b/i.test(text)) {
                    return [
                        { text: "Sorry, the amount you have entered is too low. What is your desired loan amount?" }
                    ]
                } else {
                    return [
                        { text: "Sorry, i didn't quite get that. What is your desired loan amount?" }
                    ]
                }

                // Fallback to general
            }
        },
        'loan?loan_tenure': {
            parse(text) {
                if (/\b(cancel)\b/i.test(text)) {
                    Bot.state = "general";
                    return [
                        { text: "Oh, I thought you mentioned that you wanted a loan. My bad." }
                    ]
                }

                if (/\b(12|24|36|48|60)\b/i.test(text)) {
                    Bot.state = "general";
                    return [
                        { text: "Your loan application has been filed. You will be notified of the outcome within the next 7 working days." },
                        { text: "Hey, it's me Bob again. Anything I can help you with?", delay: 5000 }
                    ]
                } else {
                    return [
                        { text: "Sorry, i didn't quite get that. What is your desired tenure duration?\n• 12 months\n• 24 months\n• 36 months\n• 48 months\n• 60 months" }
                    ]
                }

                // Fallback to general
            }
        },
    },
}


function pickRandom(arr) {
    let rand = Math.random();
    //rand = 0.78;
    rand *= arr.length; //(5)
    //rand = 3.9
    rand = Math.floor(rand);
    //rand = 3

    return arr[rand];
}


export { Bot };