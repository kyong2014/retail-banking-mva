module.exports = [
  {
    _id: Math.round(Math.random() * 1000000),
    text: 'There are 4 different kinds of loans................................................',
    createdAt: new Date(Date.UTC(2018, 2, 1, 9, 41, 0)),
    user: {
      _id: 2,
      name: 'MVA',
    },
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: 'Tertiary. I want to enrol into Singapore Polytechnic.'
    ,
    createdAt: new Date(Date.UTC(2018, 2, 1, 9, 40, 0)),
    user: {
      _id: 1,
      name: 'Anita Suresh',
    },
    sent: true,
    received: true,
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: 'What is the level of qualification you are looking for?',
    createdAt: new Date(Date.UTC(2018, 2, 1, 9, 36, 0)),
    user: {
      _id: 2,
      name: 'MVA',
    },
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: 'School Loans'
    ,
    createdAt: new Date(Date.UTC(2018, 2, 1, 9, 35, 0)),
    user: {
      _id: 1,
      name: 'Anita Suresh',
    },
    sent: true,
    received: true,
    // location: {
    //   latitude: 48.864601,
    //   longitude: 2.398704
    // },
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: 'Yes, there are secured and unsecured loans for purposes pertaining to housing, school, personal loans and many more. May I know which loan are you looking for?',
    createdAt: new Date(Date.UTC(2018, 2, 1, 9, 33, 0)),
    user: {
      _id: 2,
      name: 'Bob',
    },
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: 'Hello Bob. Can I check the types of loans available right now?'
    ,
    createdAt: new Date(Date.UTC(2018, 2, 1, 9, 33, 0)),
    user: {
      _id: 1,
      name: 'Anita Suresh',
    },
    sent: true,
    received: true,
    // location: {
    //   latitude: 48.864601,
    //   longitude: 2.398704
    // },
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: 'Welcome to HSBC Mobile Banking! I am Bob, your Mobile Virtual Assisntat (MVA).',
    createdAt: new Date(),
    user: {
      _id: 2,
      name: 'Bob',
    },
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: "You are currently talking to Bob, your personal MVA.",
    createdAt: new Date(Date.UTC(2018, 2, 19, 6, 30, 0)),
    system: true,
  },
];