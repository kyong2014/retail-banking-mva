import React, { Component } from 'react';
import { Platform, Text, View, Image, TouchableOpacity, KeyboardAvoidingView, AsyncStorage, Alert, BackHandler, } from 'react-native';
import { AppLoading, Font } from 'expo';
import { Toast } from 'native-base';
import { NavigationActions } from 'react-navigation';
import LoadingIndicator from '../components/LoadingIndicator';
import LoginForm from './components/LoginForm';
import EmailLogin from './components/EmailLogin';

var api = require('../../api');
var style_theme = require('../stylesheets/theme');
var style_login = require('../stylesheets/loginScreen');

export default class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fontLoaded: false,
            isLoading: false,
        };
    }

    async componentDidMount() {
        this.getAuth();
        BackHandler.addEventListener('backPress', this.handleBackButton);
        await Font.loadAsync({
            'Roboto_medium': require('../resources/fonts/Roboto/Roboto-Medium.ttf'),  
            'Open_Sans': require('../resources/fonts/Open_Sans/OpenSans-Regular.ttf'),
            'Open_Sans_bold': require('../resources/fonts/Open_Sans/OpenSans-Bold.ttf'),
            'Open_Sans_light': require('../resources/fonts/Open_Sans/OpenSans-Light.ttf'),
        });
    
        this.setState({ fontLoaded: true });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('backPress', this.handleBackButton);
    }

    handleBackButton() {
        return true;
    }

    onError(errMsg) {
        Toast.show({
            text: errMsg,
            position: 'bottom',
            buttonText: 'Okay'
          })
    }

    render(){
        return(
            this.state.fontLoaded? (
            <KeyboardAvoidingView behavior="padding" style={style_theme.styles.wrapper}>
                    <View style={style_theme.styles.logoContainer}>
                        <Image 
                        source={require('../resources/img/hsbc-logo.png')}
                        style={style_theme.styles.logoSmall}/>
                    </View> 

                    <LoginForm navigator={this.props.navigation}/>
                    
                   {/* <EmailLogin navigator={this.props.navigation} action={this.emailLoginBack}/> */}
                    <LoadingIndicator isLoading={this.state.isLoading}/>
            </KeyboardAvoidingView>
            ) : null
        );
    }

    emailLoginBack = (data) => {
        if(data == 'RegisterScreen'){            
            this.registerRedirect();
        }else{
            this.setAuth(data);
        }
    }

    registerRedirect = () => {
        this.props.navigation
            .navigate('RegisterScreen', { onSuccess: this.setAuth });
    }

    setAuth = async (hashAuth) => {
        if(hashAuth){
            try {
                await AsyncStorage.setItem('@userHashAuth:key', hashAuth);
                this.getAuth();
            } catch (error) {
                this.onError('Error saving hash');
            }
        }else{
            this.onError('Error saving hash');
        }
    }

    getAuth = async () => {
        try {
            const value = await AsyncStorage.getItem('@userHashAuth:key');
            if (value !== null){
                this.checkAuth(value);
            }
        } catch (error) {
            this.onError('Error retrieving hash / empty');
        }
    }

    checkAuth = (value) => {
        this.setState({isLoading: true});
        return fetch(api.API_SERVER_URL + api.GET_AUTH_TOKEN, {
            method: 'GET',
            headers: {
                'Authorization': value
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({isLoading: false});
            if(!responseJson.error){
                this.props.navigation.navigate('Banking', {
                    userData: responseJson.userData
                });
            }else{
            }
        })
        .catch((error) => {
            this.setState({isLoading: false});
            this.onError('Failed connect to server');
        });
    }
}