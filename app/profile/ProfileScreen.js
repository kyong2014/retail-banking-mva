import React from 'react';
import { Text, View, Button, Image, StyleSheet, KeyboardAvoidingView } from 'react-native';
import { Container } from 'native-base'
import { AppLoading, Font } from 'expo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CustomHeader from '../components/CustomHeader'
import ProfileForm from './components/ProfileForm'

export default class ProfileScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fontLoaded: false,
        };
    }

    async componentDidMount() {
        await Font.loadAsync({
          'Roboto_medium': require('../resources/fonts/Roboto/Roboto-Medium.ttf'),  
          'Open_Sans': require('../resources/fonts/Open_Sans/OpenSans-Regular.ttf'),
          'Open_Sans_bold': require('../resources/fonts/Open_Sans/OpenSans-Bold.ttf'),
          'Open_Sans_light': require('../resources/fonts/Open_Sans/OpenSans-Light.ttf'),
        });
    
        this.setState({ fontLoaded: true });
    }

    static navigationOptions = {
        tabBarLabel: 'Profile',
        drawerIcon: ({tintColor}) => {
            return (
                <MaterialIcons
                name="account-circle"
                size={24}
                style={{color: tintColor}}
                >
                </MaterialIcons>
            );
        }
    };

    render(){
        return(
            this.state.fontLoaded ? (
            <Container>
                <CustomHeader menu='yes' name="Profile" cartAvail='yes' showName={true} nav={this.props.navigation}/>
                <KeyboardAvoidingView>
                    <View style={styles.formContainer}>
                        <ProfileForm navigator={this.props.navigation}/>
                    </View>
                </KeyboardAvoidingView>
            </Container>
            ) : null
        );

    }
    
}

const styles = StyleSheet.create({
    formContainer: {
        marginTop: 20,
        marginRight: 5,
        marginLeft: 5,
        marginBottom: 50
    }
});
