import React, { Component } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DatePicker from 'react-native-datepicker';

var api = require('../../../api');

import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Alert,
    Button,
    Image,
    Keyboard,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { ImagePicker } from 'expo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import LoadingIndicator from '../../components/LoadingIndicator';
import { Toast } from 'native-base';

var style_theme = require('../../stylesheets/theme');
var style_header = require('../../stylesheets/customHeader');
var style_register = require('../../stylesheets/registerScreen');
var v = require('../../stylesheets/variables');

export default class ProfileForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            successfulMessage:null,

            firstname: null,
            firstnameValid: null,
            firstnameErrorMessage: '',
            firstnameEditing: false,

            lastname: null,
            lastnameValid: null,
            lastnameErrorMessage: '',
            lastnameEditing: false,

            email: null,
            emailValid: null,
            emailErrorMessage: '',
            emailEditing: false,

            mobile: null,
            mobileValid: null,
            mobileErrorMessage: '',
            mobileEditing: false,

            password: null,
            passwordEntered: null,
            passwordValid: null,
            passwordErrorMessage: '',
            passwordEditing: false,

            newpassword: null,
            newpasswordValid: null,
            newpasswordErrorMessage: '',

            newpasswordc: null,
            newpasswordcValid: null,
            newpasswordcErrorMessage: '',

            submissionError: '',

            isLoading: false,

            customer_id: null,

            is_newPassword: false,

            has_oldPassword: true,

            profilePicture: null,

            is_newPicture: false,

            gender: 1,

            show_select_gender: false
        }

    }

    async componentDidMount() {
        this.getProfile();
    }

    getProfile = async () =>{
        try {
            const value = await AsyncStorage.getItem('@userHashAuth:key');
            if (value !== null){
                this.setState({isLoading: true});
                return fetch(api.API_SERVER_URL + api.PROFILE_URL, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        token: value
                    }),
                  })
                  .then((response) => response.json())
                  .then((responseJson) => {
                      
                        this.setState({isLoading: false});

                        this.setState({
                            firstname: responseJson.firstname,
                            lastname: responseJson.lastname,
                            email: responseJson.email,
                            mobile: responseJson.mobile_no,
                            customer_id: responseJson.customer_id,
                            gender: responseJson.gender,
                            profilePicture: responseJson.avatar_file_name,
                            show_select_gender: (responseJson.gender == -1? true: false)
                        });
                        
                        if(responseJson.has_oldPassword == 1){
                            this.state.has_oldPassword = true;
                            this.state.is_newPassword = false;
                        }else{
                            this.state.has_oldPassword = false;
                            this.state.is_newPassword = true;
                        }

                        this.setState({password: ''});
                        this.setState({newpassword: ''});
                        this.setState({newpasswordc: ''});

                        this.handleInput(responseJson.firstname, 'firstname');
                        this.handleInput(responseJson.lastname, 'lastname');
                        this.handleInput(responseJson.email, 'email');
                        this.handleInput(responseJson.date_of_birth, 'dob');
                        this.handleInput(responseJson.mobile_no, 'mobile');
                  })
                  .catch((error) => {
                      this.setState({isLoading: false});
                      this.onError('Failed to connect to server');
                  });
            }
        } catch (error) {
            this.onError('Error retrieving profile data');
        }
    }

    onError(errMsg) {
        Toast.show({
            text: errMsg,
            position: 'bottom',
            buttonText: 'Okay'
          })
    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
        });
    
        if (!result.cancelled) {
          this.setState({ profilePicture: result.uri, is_newPicture: true });
        }
    };

    _saveImage = async () => {
        var uri = this.state.profilePicture;

        let uriParts = uri.split('.');
        let fileType = uriParts[uriParts.length - 1];

        const value = await AsyncStorage.getItem('@curUser');
        const user = JSON.parse(value);
        
        let formData = new FormData();
        formData.append('photo', {
            uri,
            name: `${user.customer_id}.${fileType}`,
            type: `image/${fileType}`,
        });
        
        return fetch(api.API_SERVER_URL + api.PROFILE_PICTURE_URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: formData,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({isLoading: false});
            if(!responseJson.error){
                this.setState({ profilePicture: api.API_SERVER_URL + responseJson.filename, is_newPicture: false });
                //Set the new dp in userData
                this.props.navigator.state.params.userData.avatar_file_name = this.state.profilePicture;
            }else{
                this.onError(responseJson.error);
            }
            
        })
        .catch((error) => {
            this.setState({isLoading: false});
            console.error(error);
            this.setState({submissionError: error});
        });
    }

    render() {
        
        let { profilePicture } = this.state;

        return (
            <KeyboardAwareScrollView>
                <View style={styles.wrapper}>
                    <View style={{ 
                        flex: 1, 
                        alignItems: 'center', 
                        justifyContent: 'center', 
                        marginVertical: 10, 
                        marginTop: 20 
                        }}>
                        <TouchableOpacity onPress={this._pickImage}>
                            {<Image source={
                                    profilePicture? (profilePicture.indexOf('http') != -1? {uri: profilePicture}:{uri: api.API_SERVER_URL+profilePicture}): require("../../resources/img/hsbc-logo.png")
                                } style={{ height: 80, width: 80, borderRadius: 40 }}/>}
                        </TouchableOpacity>
                        <TouchableOpacity  style={[this.state.is_newPicture? null:{display: 'none'}]} onPress={this._saveImage.bind(this)}>
                            <Text>Save Picture</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={[styles.profileTitle]}>
                        {this.state.firstname ? this.state.firstname: ''} {this.state.lastname ? this.state.lastname: ''}
                    </Text>

                    <Text style={[ 
                            styles.errorMessage, 
                            ( this.state.firstnameValid !== false ) ? styles.hidden : null 
                        ]}>
                        {this.state.firstnameErrorMessage}
                    </Text>
                    
                    { this.state.firstnameEditing ? (
                        <TextInput style={style_theme.styles.input}
                            placeholder={firstnamePlaceholder}
                            placeholderTextColor="rgba(0,0,0, 0.50)"
                            underlineColorAndroid={'transparent'}
                            keyboardType="email-address"
                            autoCapitalize="none"
                            autoCorrect={false}
                            value={this.state.firstname}
                            onChangeText={(text) => this.setState({firstname: text})}
                            onBlur={(text) => this.handleInput(this.state.firstname, 'firstname')}
                        />
                    ) : (
                        <TouchableOpacity style={[
                            style_theme.styles.input,
                            {   
                                backgroundColor: '#ccc',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }]}
                            onPress = {() => {this.setState({firstnameEditing: true})}}
                            >
                            <Text style={{ fontSize: v.P_FONTSIZE, color: 'grey' }}>
                                {this.state.firstname}
                            </Text>
                            <MaterialIcons 
                                name="mode-edit" 
                                style={style_header.styles.icon} 
                                size={24}
                            />
                        </TouchableOpacity>
                    )}
                    
                    <Text style={[ 
                            styles.errorMessage, 
                            ( this.state.lastnameValid !== false ) ? styles.hidden : null 
                        ]}>
                        {this.state.lastErrorMessage}
                    </Text>
                    { this.state.lastnameEditing ? (
                        <TextInput style={style_theme.styles.input}
                            placeholder={lastnamePlaceholder}
                            placeholderTextColor="rgba(0,0,0, 0.50)"
                            underlineColorAndroid={'transparent'}
                            keyboardType="email-address"
                            autoCapitalize="none"
                            autoCorrect={false}
                            value={this.state.lastname}
                            onChangeText={(text) => this.setState({lastname: text})}
                            onBlur={(text) => this.handleInput(this.state.lastname, 'lastname')}
                        />
                    ) : (
                        <TouchableOpacity style={[
                            style_theme.styles.input,
                            {   
                                backgroundColor: '#ccc',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }]}
                            onPress = {() => {this.setState({lastnameEditing: true})}}
                            >
                            <Text style={{ fontSize: v.P_FONTSIZE, color: 'grey' }}>
                                {this.state.lastname}
                            </Text>
                            <MaterialIcons 
                                name="mode-edit" 
                                style={style_header.styles.icon} 
                                size={24}
                            />
                        </TouchableOpacity>
                    )}

                    <Text style={[ 
                            styles.errorMessage, 
                            ( this.state.emailValid !== false ) ? styles.hidden : null 
                        ]}>
                        {this.state.emailErrorMessage}
                    </Text>
                    
                    { this.state.emailEditing ? (
                        <TextInput style={style_theme.styles.input}
                            placeholder={idPlaceholder}
                            placeholderTextColor="rgba(0,0,0, 0.50)"
                            underlineColorAndroid={'transparent'}
                            keyboardType="email-address"
                            autoCapitalize="none"
                            autoCorrect={false}
                            value={this.state.email}
                            onChangeText={(text) => this.setState({email: text})}
                            onBlur={(text) => this.handleInput(this.state.email, 'email')}
                        />
                    ) : (
                        <TouchableOpacity style={[
                            style_theme.styles.input,
                            {   
                                backgroundColor: '#ccc',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }]}
                            onPress = {() => {this.setState({emailEditing: true})}}
                            >
                            <Text style={{ fontSize: v.P_FONTSIZE, color: 'grey' }}>
                                { this.state.email ? this.state.email : "Email address" }
                            </Text>
                            <MaterialIcons 
                                name="mode-edit" 
                                style={style_header.styles.icon} 
                                size={24}
                            />
                        </TouchableOpacity>
                    )}

                    <Text style={[ 
                            styles.errorMessage, 
                            ( this.state.mobileValid !== false ) ? styles.hidden : null 
                        ]}>
                        {this.state.mobileErrorMessage}
                    </Text>
                    
                    { this.state.mobileEditing ? (
                        <TextInput style={style_theme.styles.input}
                            placeholder={mobilePlaceholder}
                            placeholderTextColor="rgba(0,0,0, 0.50)"
                            underlineColorAndroid={'transparent'}
                            keyboardType="phone-pad"
                            autoCapitalize="none"
                            autoCorrect={false}
                            maxLength={8}
                            value={ this.state.mobile}
                            onChangeText={(text) => this.setState({mobile: text})}
                            onBlur={(text) => this.handleInput(this.state.mobile, 'mobile')}
                        />
                    ) : (
                        <TouchableOpacity style={[
                            style_theme.styles.input,
                            {   
                                backgroundColor: '#ccc',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }]}
                            onPress = {() => {this.setState({mobileEditing: true})}}
                            >
                            <Text style={{ fontSize: v.P_FONTSIZE, color: 'grey' }}>
                                {this.state.mobile ? this.state.mobile : "Mobile no." }
                            </Text>
                            <MaterialIcons 
                                name="mode-edit" 
                                style={style_header.styles.icon} 
                                size={24}
                            />
                        </TouchableOpacity>
                    )}
      
                    <Text style = {this.state.show_select_gender?null: {display:'none'}}>Select your Gender</Text>
                    <View style={[{ display: 'flex', flexDirection: 'row', marginTop: 10}, this.state.show_select_gender?null: {display:'none'}]}>
                        
                        <TouchableOpacity style={[
                            style_theme.styles.buttonCentered,
                            { width: v.BUTTON_WIDTH/2 - 15, marginRight: 30 },
                            this.state.gender === 1 ? style_register.styles.selected : style_register.styles.deselected ,
                            ]}
                            onPress={this.selectMale}>
                            <Text style={[style_theme.styles.buttonText, style_theme.styles.centeredText]}>Male</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[
                            style_theme.styles.buttonCentered,
                            { width: v.BUTTON_WIDTH/2 - 15 },
                            this.state.gender === 0 ? style_register.styles.selected : style_register.styles.deselected ,
                            ]}
                            onPress={this.selectFemale}>
                            <Text style={[style_theme.styles.buttonText, style_theme.styles.centeredText]}>Female</Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={[ 
                            styles.errorMessage, 
                            ( this.state.passwordValid !== false )? styles.hidden : null
                        ]}>
                        {this.state.passwordErrorMessage}
                    </Text>
                    
                    { this.state.passwordEditing ? (
                        <View>
                            {this.state.has_oldPassword? (
                                <TextInput style={style_theme.styles.input}
                                    placeholder={pwPlaceholder}
                                    placeholderTextColor="rgba(0,0,0, 0.50)"
                                    underlineColorAndroid={'transparent'}
                                    secureTextEntry
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    value={this.state.password}
                                    onChangeText={(text) => this.setState({password: text})}
                                    onBlur={(text) => this.handleInput(this.state.password, 'password')}
                                />
                            ) : null
                            }

                            {/* New Password? Button */}
                            <View style={[{ display: 'flex', flexDirection: 'row' }, this.state.has_oldPassword? null: {display:'none'}]}>
                                
                                <TouchableOpacity style={[
                                    styles.newPasswordButton, 
                                    this.state.is_newPassword ? { backgroundColor: '#090' } : null
                                    ]}
                                    onPress={this._changePassword}>
                                    <Text style={styles.buttonText}>Change Password</Text>
                                </TouchableOpacity>

                            </View>

                            <TextInput style={[style_theme.styles.input, this.state.is_newPassword ? null : {display: 'none'}]}
                                placeholder={newpwPlaceholder}
                                placeholderTextColor="rgba(0,0,0, 0.50)"
                                underlineColorAndroid={'transparent'}
                                secureTextEntry
                                autoCapitalize="none"
                                autoCorrect={false}
                                value={this.state.newpassword}
                                onChangeText={(text) => this.setState({newpassword: text})}
                                onBlur={(text) => this.handleInput(this.state.newpassword, 'newpassword')}
                            />

                            <TextInput style={[style_theme.styles.input, this.state.is_newPassword ? null : {display: 'none'}]} 
                                placeholder={newpwcPlaceholder}
                                placeholderTextColor="rgba(0,0,0, 0.50)"
                                underlineColorAndroid={'transparent'}
                                secureTextEntry
                                autoCapitalize="none"
                                autoCorrect={false}
                                value={this.state.newpasswordc}
                                onChangeText={(text) => this.setState({newpasswordc: text})}
                                onBlur={(text) => this.handleInput(this.state.newpasswordc, 'newpasswordc')}
                            />
                        </View>
                    ) : (
                        <TouchableOpacity style={[
                            style_theme.styles.input,
                            {   
                                backgroundColor: '#ccc',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                            }]}
                            onPress = {() => {this.setState({passwordEditing: true})}}
                            >
                            <Text style={{ fontSize: v.P_FONTSIZE, color: 'grey' }}>
                                { this.state.has_oldPassword ? "********" : "No password set" }
                            </Text>
                            <MaterialIcons 
                                name="mode-edit" 
                                style={style_header.styles.icon} 
                                size={24}
                            />
                        </TouchableOpacity>
                    )}
                    
                    {/* Submit Button */}
                    <Text style={[
                        styles.successfulMessage, 
                        this.state.successfulMessage ? null : {display: 'none' 
                        }]}> 
                        {this.state.successfulMessage} 
                    </Text>
                    <Text style={[
                        styles.errorMessage, 
                        this.state.submissionError ?  null : {display: 'none' 
                        }]}>
                        {this.state.submissionError}
                    </Text>
                    <TouchableOpacity style={style_theme.styles.buttonCentered}
                        onPress={this.handleSubmit} >
                        <Text style={[style_theme.styles.buttonText, style_theme.styles.centeredText]}>Update Details</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style_theme.styles.buttonCentered}
                        onPress={this.logout} >
                        <Text style={[style_theme.styles.buttonText, style_theme.styles.centeredText]}>Log Out</Text>
                    </TouchableOpacity>
                    <LoadingIndicator isLoading={this.state.isLoading}/>
                    
                </View>
            </KeyboardAwareScrollView>
        );
    }
    
    handleInput(text,field){
        switch(field) {
            case 'firstname':
                if(/[A-z]+/.test(text)) {
                    this.setState({ 
                        firstnameValid: true,
                        firstnameErrorMessage: ''
                    });
                } else {
                    this.setState({
                        firstnameValid: false,
                        nameErrorMessage: "Please write your full name",
                        //firstnameErrorMessage: this.state.firstname
                    });
                }
                break;
            case 'lastname':
                if(/[A-z]+/.test(text)) {
                    this.setState({ 
                        lastnameValid: true,
                        lastnameErrorMessage: ''

                    });
                } else {
                    this.setState({
                        lastnameValid: false,
                        nameErrorMessage: "Please write your full name",
                        //lastnameErrorMessage: this.state.lastname
                    });
                }
                break;
            case 'email':
                if(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,})+)$/.test(text)) { 
                    this.setState({ 
                        emailValid: true,
                        emailErrorMessage: '',
                    });
                } else {
                    this.setState({
                        emailValid: false,
                        emailErrorMessage: "Please use a valid email address",
                        //emailErrorMessage: this.state.email
                    });
                }
                break;
            case 'mobile':
                if(/\d{8}/.test(text)) { 
                    this.setState({ 
                        mobileValid: true,
                        mobileErrorMessage: ''
                    });
                } else {
                    this.setState({
                        mobileValid: false,
                        mobileErrorMessage: "Please use a valid Phone Number",
                        //mobileErrorMessage: this.state.mobile
                    });
                }
                break;
            case 'password':
                this.setState( { passwordEntered: true });
                break;
            case 'newpassword':
                if(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(text)) { // 8 chars, 1 number, 1 Uppercase, 1 lower, 1 special and the blood of a virgin
                    this.setState({ 
                        newpasswordValid: true,
                        newpasswordErrorMessage: ''
                    });

                } else {
                    this.setState({
                        newpasswordValid: false,
                        newpasswordErrorMessage: "New Password must be 8 characters long, have 1 upper and lower case character, 1 number and 1 special character",
                        //passwordErrorMessage: this.state.password
                    });
                }
                break;
            case 'newpasswordc':
                if(this.state.newpassword === text) {
                    this.setState({ 
                        newpasswordcValid: true,
                        newpasswordcErrorMessage: ''
                    });
                } else {
                    this.setState({
                        newpasswordcValid: false,
                        newpasswordcErrorMessage: "New Passwords do not match. Try again? ",
                        //passwordcErrorMessage: this.state.passwordc
                    });
                }
                break;
            default:
                break;
        }

    }

    _changePassword = () => {
        this.setState({is_newPassword: !this.state.is_newPassword});
    }

    checkvalidation(){
        if(!this.state.has_oldPassword){
            if(this.state.newpasswordValid && this.state.newpasswordcValid){
                
                return true

            }else{
                
                return false
            }
        }else{
            if(this.state.passwordEntered){

                return true
            }else{

                return false
            }
        }
    }

    selectMale = () => {
        this.setState({gender: 1});
    }

    selectFemale = () => {
        this.setState({gender: 0});
    }

    handleSubmit = () => {
        // check all fields
        Keyboard.dismiss();
        if(
            this.state.firstnameValid && 
            this.state.lastnameValid && 
            this.state.emailValid &&
            this.state.mobileValid && this.checkvalidation()
        ) {

            this.setState({submissionError: ''});
            this.setState({isLoading: true});
            // SUBMIT TO DATABASE
            return fetch(api.API_SERVER_URL + api.PROFILE_URL, {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    firstname: this.state.firstname,
                    lastname: this.state.lastname,
                    email: this.state.email,
                    password: this.state.password,
                    newpassword: this.state.newpassword,
                    mobile_no: this.state.mobile,
                    has_oldPassword: this.state.has_oldPassword,
                    is_newPassword: this.state.is_newPassword,
                    customer_id: this.state.customer_id,
                    gender: this.state.gender
                }),
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({isLoading: false});
                if(!responseJson.error){
                    this.getProfile();
                    this.setState({
                    passwordValid: true,
                    successfulMessage: "Successfully Updated!"})
                }else{
                    this.setState({
                        passwordValid: false,
                        passwordErrorMessage: "Invalid Password ",
                        })
                }
                
            })
            .catch((error) => {

                this.setState({isLoading: false});
                this.setState({submissionError: error});
            });

        } else {

            this.setState({submissionError: 'Please fill in all fields'});
        }
    }

    logout = async () => {
        Alert.alert(
            'Are you sure you wish to Logout?',
            this.state.lastScannedUrl,
            [
                {
                    text: 'Yes',
                    onPress: async () => {
                        try {
                            await AsyncStorage.removeItem('@userHashAuth:key');
                            await AsyncStorage.setItem('@curItems', "");
                            await AsyncStorage.setItem('@curStore', "");
                            this.props.navigator.navigate('LoginScreen');
                        } catch (error) {
                            // this.onError(error);
                            // this.onError('Error occur when logout!!!');
                        }
                    },
                },
                { text: 'No', onPress: () => { } },
            ],
            { cancellable: false }
        );

    }




}

const styles = StyleSheet.create({
    successfulMessage:{
        color : 'grey',
        marginBottom: 20,
        width: 250
    },
    wrapper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileTitle: {
        fontWeight: 'bold',
        fontSize: 24,
        paddingBottom: 20
    },
    input: {
        backgroundColor: 'rgba(255,255,255, 0.30)',
        height: 50,
        width: 270,
        marginBottom: 20,
        borderRadius: 10,
        padding: 10
    },

    date: {
        backgroundColor: 'rgba(255,255,255, 0.30)',
        height: 50,
        width: 270,
        marginBottom: 20,
        borderRadius: 10,
        padding: 10
    },

    buttonContainer: {
        backgroundColor: 'rgba(232, 72,95,1)',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        alignSelf: 'stretch',
        borderRadius: 10,
        marginBottom: 60
    },

    genderButton: {
        backgroundColor: '#999',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        alignSelf: 'stretch',
        borderRadius: 10,
        marginTop: 10,
        marginBottom: 20,
        width: 115
    },

    newPasswordButton: {
        backgroundColor: '#999',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        alignSelf: 'stretch',
        borderRadius: 10,
        marginTop: 10,
        marginBottom: 20,
        width: 270
    },

    buttonText: {
        color: 'white'

    },

    profileRedirect: {
        marginTop: 20,
    },

    errorMessage: {
        color: 'red',
        marginBottom: 20,
        width: 250
    },

    hidden: {
        marginBottom: 0,
        height: 0,
        width: 0,
        opacity: 0,
    }
});

const firstnamePlaceholder = "First Name";
const lastnamePlaceholder = "Last Name";
const idPlaceholder = "Email address";
const mobilePlaceholder = "Mobile No.";
const pwPlaceholder = "Current Password";
const newpwPlaceholder = "New Password (8 Characters, At least 1 Upper and Lower Case, and a special Character) ";
const newpwcPlaceholder = "Confirm New Password";