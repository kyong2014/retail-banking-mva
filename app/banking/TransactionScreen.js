import React from 'react';
import { Text, View, Button, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Container, Content } from 'native-base';
import { AppLoading, Font } from 'expo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CustomHeader from './../components/CustomHeader';
import BankingTransactions from './BankingTransactions';

export default class TransactionScreen extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'Payment',
        drawerIcon: ({tintColor}) => {
            return (
                <MaterialIcons
                name="credit-card"
                size={24}
                style={{color: tintColor}}
                >
                </MaterialIcons>
            );
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            fontLoaded: false,
        };
    }

    async componentDidMount() {
        await Font.loadAsync({
          'Roboto_medium': require('../resources/fonts/Roboto/Roboto-Medium.ttf'),  
          'Open_Sans': require('../resources/fonts/Open_Sans/OpenSans-Regular.ttf'),
          'Open_Sans_bold': require('../resources/fonts/Open_Sans/OpenSans-Bold.ttf'),
          'Open_Sans_light': require('../resources/fonts/Open_Sans/OpenSans-Light.ttf'),
        });
    
        this.setState({ fontLoaded: true });
    }

    render(){
        const { navigate } = this.props.navigation;
        return(
            this.state.fontLoaded ? (
            <Container>
                <CustomHeader menu='yes' name="Bank Transactions" cartAvail='yes' showName={true} nav={this.props.navigation}/>
                <Content>
                    <BankingTransactions navitator={this.props.navigation}/>
                </Content>
            </Container>
            ) : null
        );

    }
}
