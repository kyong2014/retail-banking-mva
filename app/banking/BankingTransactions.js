import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Separator, Body, Card, CardItem, connectStyle, Thumbnail, Icon, Left, Right } from 'native-base';
import Swipeable from 'react-native-swipeable';
import {
  AppRegistry,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Alert,
  Image,
  AsyncStorage,
  TouchableHighlight
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import CustomHeader from './../components/CustomHeader'
import { AppLoading, Font } from 'expo'
import QRCode from 'react-native-qrcode';
import LoadingIndicator from './../components/LoadingIndicator';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

var api = require('./../../api');

var style_theme = require('./../stylesheets/theme');
var style_drawer = require('./../stylesheets/drawerContent');

const transactionsList =
[
    {
        "category": "NTUC Grocery Store",
        "date": "12 Jan 2018 12:34",
        "total": "-$63.20"
    },
    {
        "category": "SMU School Fees",
        "date": "12 Feb 2018 10:57",
        "total": "-$5,500.15"
    },
    {
        "category": "Petrol",
        "date": "10 Feb 2018 17:34",
        "total": "-$55.60"
    },
    {
        "category": "Uber Pool",
        "date": "10 Jan 2018 22:34",
        "total": "-$23.20"
    },
    {
        "category": "Grab Share",
        "date": "1 Mar 2018 04:24",
        "total": "-$13.40"
    },
    {
        "category": "Red Wings",
        "date": "26 Jan 2018 17:34",
        "total": "-$420.30"
    },
    {
        "category": "Uniqlo",
        "date": "28 Jan 2018 19:34",
        "total": "-$52.65"
    },
    {
        "category": "Jet Star",
        "date": "26 Feb 2018 11:34",
        "total": "-$320.00"
    },
    {
        "category": "ATM Withdrawal",
        "date": "02 Feb 2018 11:20",
        "total": "-$100.00"
    },
]

class BankingTransactions extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fontLoaded: false,
            isLoading: false,
            paymentMethods: []
        };
    }
    
 
    async componentDidMount() {
        await Font.loadAsync({
            'Open_Sans_bold': require('./../resources/fonts/Open_Sans/OpenSans-Bold.ttf'),
            'Roboto_medium': require('./../resources/fonts/Roboto/Roboto-Medium.ttf'),  
        });
        this.setState({ fontLoaded: true });
    }

    render() {
        const styles = this.props.style;
        return (
            this.state.fontLoaded ? (
                <Container>
                    <Content>
                        <List>
                            <ListItem>
                                <View style={{flex: 3,alignItems: 'flex-start'}}>
                                    <Text style={{color: '#404040'}}>Current Account</Text>
                                    <Text note>123-123456-123</Text>
                                    <Text style={{color: '#404040'}}>$55,939.45</Text>
                                </View>
                                <Right>
                                    <Icon name="arrow-forward" style={{fontSize: 20, color: '#404040'}}/>
                                </Right>
                            </ListItem>
                        </List>
                        <List dataArray={transactionsList} renderRow={(transaction) =>
                            <ListItem>
                                <View style={{flex: 3,alignItems: 'flex-start'}}>
                                    <Text style={{color: '#404040'}}>{transaction.category}</Text>
                                    <Text note>{transaction.date}</Text>
                                </View>
                                <View style={{flex: 1,alignItems: 'flex-end'}}>
                                    <Text style={{color: '#404040'}}>{transaction.total}</Text>
                                    <Text></Text>
                                </View>
                                <Right>
                                    <Icon name="arrow-forward" style={{fontSize: 20, color: '#404040'}}/>
                                </Right>
                            </ListItem>
                        }>
                        </List>
                        <List>
                            <ListItem>
                                <View style={{flex: 1,alignItems: 'flex-start'}}>
                                    <Text style={{color: '#404040', alignContent: 'center'}}>
                                        Pay Bill
                                    </Text>
                                </View>
                                <View style={{flex: 1,alignItems: 'flex-start'}}>
                                    <Text style={{color: '#404040', alignContent: 'center'}}>
                                        Make Transfer
                                    </Text>
                                </View>
                            </ListItem>
                        </List>
                    </Content>
                    <LoadingIndicator isLoading={this.state.isLoading}/>
                </Container>
            ) : null
        )
    }
}



const styles = {

}

export default connectStyle('yourTheme.BankingTransactions', styles)(BankingTransactions);


