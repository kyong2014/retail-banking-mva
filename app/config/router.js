import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';
import MVAScreen from '../screens/MVAScreen';

export const Tabs = TabNavigator({
    MVA: {
        screen: MVAScreen,
        navigationOptions: {
            tabBar: {
                label: 'MVA',
            }
        },
    },
    // Transaction: {
    //     screen: TransactionListScreen,
    //     navigationOptions: {
    //         tabBar: {
    //             label: 'Transaction',
    //         }
    //     },
    // },
});