import React from 'react';
import { Text, View, Button, Image, StyleSheet } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CustomHeader from '../components/CustomHeader'
import { AppLoading, Font } from 'expo';
import { Container, Header, Content, List, ListItem, Separator, Body, Card, CardItem, connectStyle, Thumbnail, Icon, Left, Right } from 'native-base';
import ProductService from './ProductService';

export default class ProductServiceScreen extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'About',
        drawerIcon: ({tintColor}) => {
            return (
                <MaterialIcons
                name="description"
                size={24}
                style={{color: tintColor}}
                >
                </MaterialIcons>
            );
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            fontLoaded: false,
        };
    }

    async componentDidMount() {
        await Font.loadAsync({
            'Roboto_medium': require('../resources/fonts/Roboto/Roboto-Medium.ttf'),  
            'Open_Sans': require('../resources/fonts/Open_Sans/OpenSans-Regular.ttf'),
            'Open_Sans_bold': require('../resources/fonts/Open_Sans/OpenSans-Bold.ttf'),
            'Open_Sans_light': require('../resources/fonts/Open_Sans/OpenSans-Light.ttf'),
        });
    
        this.setState({ fontLoaded: true });
    }

    render(){
        const { navigate } = this.props.navigation;
        return(
            this.state.fontLoaded ? (
            <Container>
                <CustomHeader menu='yes' cartAvail='yes' name="Products & Services" showName={true} nav={this.props.navigation}/>
                <Content>
                    <ProductService navigator={this.props.navigation}/>
                </Content>
            </Container>
            ) : null
        );
    }
}