import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Text, Separator, Body, Card, CardItem, connectStyle, Toast, Left, Right, Icon } from 'native-base';
import { AppRegistry, StyleSheet, TouchableOpacity, View } from 'react-native';
import { NavigationActions } from 'react-navigation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class ProductService extends Component {
    constructor(props) {
        super(props);
    }

    render() { 
        return (
            <List>
                <Content>
                    <Separator>
                        <Text>Online Services</Text>
                    </Separator>

                    <Card>
                        <CardItem>
                            <View style={{flex: 3,alignItems: 'flex-start'}}>
                                <Text style={{color: '#404040', fontWeight:'bold'}}>Personal Internet Banking</Text>
                                <Text note>Take control of your finances online securely anytime, anywhere with just a click away.</Text>
                            </View>
                            <Right>
                                <Icon name="arrow-forward" style={{fontSize: 20, color: '#404040'}}/>
                            </Right>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem>
                            <View style={{flex: 3,alignItems: 'flex-start'}}>
                                <Text style={{color: '#404040', fontWeight:'bold'}}>Interactive Demo Guide</Text>
                                <Text note>Tutorial videos to take you through the online services you need the most</Text>
                            </View>
                            <Right>
                                <Icon name="arrow-forward" style={{fontSize: 20, color: '#404040'}}/>
                            </Right>
                        </CardItem>
                    </Card>

                    <Separator>
                        <Text>Wealth Management</Text>
                    </Separator>

                    <Card>
                        <CardItem>
                            <View style={{flex: 3,alignItems: 'flex-start'}}>
                                <Text style={{color: '#404040', fontWeight:'bold'}}>Invest with guidance</Text>
                                <Text note>Our financial consultants will work with you to thoroughly understand your wealth needs</Text>
                            </View>
                            <Right>
                                <Icon name="arrow-forward" style={{fontSize: 20, color: '#404040'}}/>
                            </Right>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem>
                            <View style={{flex: 3,alignItems: 'flex-start'}}>
                                <Text style={{color: '#404040', fontWeight:'bold'}}>HSBC Goal Planner</Text>
                                <Text note>An intuitive tool to ensure your wealth portfolio is effectively serving your needs</Text>
                            </View>
                            <Right>
                                <Icon name="arrow-forward" style={{fontSize: 20, color: '#404040'}}/>
                            </Right>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem>
                            <View style={{flex: 3,alignItems: 'flex-start'}}>
                                <Text style={{color: '#404040', fontWeight:'bold'}}>HSBC's Wealth Dashboard</Text>
                                <Text note>A holistic wealth management solution that gives you a real-time, consolidated view of all your holdings</Text>
                            </View>
                            <Right>
                                <Icon name="arrow-forward" style={{fontSize: 20, color: '#404040'}}/>
                            </Right>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem>
                            <View style={{flex: 3,alignItems: 'flex-start'}}>
                                <Text style={{color: '#404040', fontWeight:'bold'}}>30-day Service Pledge</Text>
                                <Text note>Come back to us within 30 days and we will waive or refund the related fees or charges for eligible products</Text>
                            </View>
                            <Right>
                                <Icon name="arrow-forward" style={{fontSize: 20, color: '#404040'}}/>
                            </Right>
                        </CardItem>
                    </Card>
                </Content>
            </List>
        );
    }
}

export default connectStyle('yourTheme.ProductService', styles)(ProductService);